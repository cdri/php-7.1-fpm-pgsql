# php-7.1-fpm-pgsql
[Coderi : Inteligência Empresarial](http://www.coderi.com.br) | [DockerHub](https://cloud.docker.com/swarm/kamihouse/repository/docker/kamihouse/php-7.1-fpm-pgsql) custom image.

### Utilities

* php-7.1-fpm
* xdebug
* pdo_pgsql (libpq-dev)
