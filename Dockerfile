FROM php:7.1-fpm

RUN apt-get update
RUN apt-get install -yq libpq-dev
RUN pecl install xdebug
RUN docker-php-ext-enable xdebug
RUN docker-php-ext-install pdo_pgsql

CMD ["php-fpm"]
